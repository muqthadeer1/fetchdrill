// using the fetch function provided by the Fetch API.
const todosData = fetch('https://jsonplaceholder.typicode.com/todos');

//fetching data from todo by then and catch method.
todosData
.then((response)=>{
    if(response.ok){
        return response.json();
    } else {
        throw new Error("Error Message");
    }
})
.then((todosDetails)=>{
    console.log(todosDetails)
})
.catch(error=>{
    console.log(error);
})