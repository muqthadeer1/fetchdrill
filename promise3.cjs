// using the fetch function provided by the Fetch API Getting the data by url
const usersData = fetch('https://jsonplaceholder.typicode.com/users');
const todosData = fetch('https://jsonplaceholder.typicode.com/todos');


//getting the Data by then and catch method
usersData
.then((response)=>{
    if(response.ok){
        return response.json();
    } else {
        throw new Error("Error");
    }
})
.then((data)=>{
    console.log("users data:",data);
    return todosData;
})
.then((response)=>{
    if(response.ok){
        return response.json();
    } else {
        throw new Error("Error");
    }
})
.then((data)=>{
    console.log("ToDos:",data);
})

