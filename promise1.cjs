  // using the fetch function provided by the Fetch API.
  const fetchAllUsers = fetch('https://jsonplaceholder.typicode.com/users');


  // fetching data by then and catch method
  fetchAllUsers
  .then((response)=>{
     if(response.ok){
        return response.json();
     } else 
     {
        throw new Error("error");
     }

  })
  .then((data)=>{
    console.log(data)
  })
  .catch(error=>{
    console.log(error);
  })

