// using the fetch function provided by the Fetch API to get data
const todosDetails = fetch('https://jsonplaceholder.typicode.com/todos');

//getting the data by using then and catch method
todosDetails
.then((response)=>{
    if(response.ok){
        return response.json();
    }else {
        console.log("error")
    }
})
.then((data)=>{
    const firstToDo = data[0].id; //
    return fetch(`https://jsonplaceholder.typicode.com/todos?userId=${firstToDo}`)
})
.then((resolve)=>{
    if(resolve.ok){
       return resolve.json();
    }

})
.then((data)=>{
    console.log(data);
})
.catch(error=>{
    console.log(error);
})



