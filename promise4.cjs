// using the fetch function provided by the Fetch API to get the data
const userData = fetch('https://jsonplaceholder.typicode.com/users');

//getting the information by using the catch and then method.
userData
.then((response)=>{
    if(response.ok){
        return response.json();
    } else {
        console.log('data not Found!');
    }
})
.then((data)=>{
    // console.log("data:",data);
    fetchData = data.map((element)=>{
        return fetch(`https://jsonplaceholder.typicode.com/users?id=${element.id}`)
        .then((resolve)=>{
            return resolve.json();
    })
    })
    return Promise.all(fetchData);
})
.then((data)=>{
    console.log(data);
})
.catch(error=>{
    console.log(error);
})